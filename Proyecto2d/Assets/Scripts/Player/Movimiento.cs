using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Movimiento : MonoBehaviour
{
    private Rigidbody2D rb;
    public static float rapidez = 3f;
    public float magnitudSalto;
    bool puedeSaltar = true;
    private float DistanciaSaltoPlataforma = 1;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        
    }

    void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float velocidadHorizontal = horizontalInput * rapidez;
        rb.velocity = new Vector2(velocidadHorizontal, rb.velocity.y);

    }
    public void Saltar(InputAction.CallbackContext context)
    {  
        if (context.performed && puedeSaltar)
        {
            rb.velocity = new Vector2(rb.velocity.x, magnitudSalto);
            puedeSaltar=false;
        }

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.gameObject.CompareTag("Plataforma"))
        {
            if (collision.gameObject.transform.position.y < gameObject.transform.position.y && 
                transform.position.x > collision.transform.position.x - DistanciaSaltoPlataforma && 
                transform.position.x < collision.transform.position.x + DistanciaSaltoPlataforma)
            {
                puedeSaltar = true;
            }
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        puedeSaltar = false;
    }
}
