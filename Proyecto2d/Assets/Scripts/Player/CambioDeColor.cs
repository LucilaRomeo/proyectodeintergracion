using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambioDeColor : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Sprite Azul;
    public Sprite Rojo;
    public Sprite Amarillo;
    public bool cambio;
    public string colorActual;

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        colorActual = "Azul";
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Joystick1Button1))
        {
            spriteRenderer.sprite = Rojo;
            colorActual = "Rojo";
            cambio = true;
        }
        if (Input.GetKeyDown(KeyCode.Joystick1Button2))
        {
            spriteRenderer.sprite = Azul;
            colorActual = "Azul";
            cambio = true;
        }
        if (Input.GetKeyDown(KeyCode.Joystick1Button3))
        {
            spriteRenderer.sprite = Amarillo;
            colorActual = "Amarillo";
            cambio = true;
        }
    }
}

   
 