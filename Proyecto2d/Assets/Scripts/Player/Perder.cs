using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Perder : MonoBehaviour
{
    private Vector3 ultimaPosicion;
    private string levelname;
    public Transform jugador;
    public EventHandler<bool> Restablecer;

    void Start()
    {
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        string collisionColor = collision.gameObject.GetComponent<Plataforma>().colorP;
        if (collisionColor != "Gris" && ValidarColor(collisionColor))
        {
            jugador.position = GameManager.instance.checkPoints[GameManager.instance.nivelActual].position + Vector3.up;
            Restablecer?.Invoke(this, true);
        }else if (collision.gameObject.CompareTag("Fin"))
        {
            jugador.position = GameManager.instance.checkPoints[GameManager.instance.nivelActual].position + Vector3.up;
            Restablecer?.Invoke(this, true);
        }
    }
    public bool ValidarColor(string color)
    {
        if ((color == "Rojo" || color == "Azul" || color == "Amarillo") && color == GetComponent<CambioDeColor>().colorActual)
        {
            return false;
        }
        else if (color == "Verde" && (GetComponent<CambioDeColor>().colorActual == "Azul" || GetComponent<CambioDeColor>().colorActual == "Amarillo"))
        {
            return false;
        }
        else if (color == "Naranja" && (GetComponent<CambioDeColor>().colorActual == "Rojo" || GetComponent<CambioDeColor>().colorActual == "Amarillo"))
        {
            return false;
        }
        else if (color == "Violeta" && (GetComponent<CambioDeColor>().colorActual == "Azul" || GetComponent<CambioDeColor>().colorActual == "Rojo"))
        {
            return false;
        }
        return true;
    }

}
