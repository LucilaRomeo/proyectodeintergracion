using System.Collections;
using UnityEngine;
using System;

public class CambioDeNivel : MonoBehaviour
{
    [SerializeField] private GameObject[] plataformasCaen;
    public float velocidadMovimiento = 5f;
    public float tiempoEspera = 2f;
    public int nivelDePlataforma;
    public GameObject BanderaVerde;
    public GameObject BanderaRoja;


    void Start()
    {
        GameManager.instance.cambioDeNivel += FinNivel;  //Se suscribe al evento de cambio de nivel
    }
    private void OnDestroy()
    {
        GameManager.instance.cambioDeNivel -= FinNivel;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        foreach (GameObject plataforma in plataformasCaen) //recorre todas las plataformas que caen
        {
            if (plataforma != null && collision.gameObject.CompareTag("Player")) //verifica que la plataforma no sea null y que el jugador collisione
            {
                if (collision.gameObject.transform.position.y > gameObject.transform.position.y && transform.position.x > collision.transform.position.x - 0.8 && transform.position.x < collision.transform.position.x + 0.8)
                {
                    //caen
                    CaidaDeObstaculos scriptCaida = plataforma.GetComponent<CaidaDeObstaculos>();
                    if (scriptCaida == null)
                        continue;
                    plataforma.GetComponent<CaidaDeObstaculos>().caen = true;
                    plataforma.GetComponent<CaidaDeObstaculos>().caenPlataformas();
                    Debug.Log("entro");
                    GameManager.instance.actualizarNivel(nivelDePlataforma); //ctualiza el nivel en el manager
                    BanderaRoja.SetActive(false);
                    BanderaVerde.SetActive(true);
                }
            }
        }
    }

    public void FinNivel(object sender, int nivel)
    {

        if (nivel < GameManager.instance.puntosIniciales.Length) //verifica que le nnivel sea menor que la cantidad de puntos inciales
        {
            StartCoroutine(GameManager.instance.MoverCamaraAlInicio(velocidadMovimiento, tiempoEspera));
        }
        else
        {
            Debug.Log("No hay m�s niveles");
        }
    }


}
