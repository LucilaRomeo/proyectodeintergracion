using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaidaDeObstaculos : MonoBehaviour
{
    public Rigidbody2D rigidbody;
    public float tiempoEspera = 2.0f;
    public  bool caen = false;
    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();

    }
    public void caenPlataformas()
    {
        if (caen == true)
        {
            rigidbody.constraints = RigidbodyConstraints2D.FreezeRotation; //se cancela la rotacion
            transform.position -= transform.up * 5 * Time.deltaTime;  //El obstaculo cae hacia abajo
            StartCoroutine(DestruirPlataformasDespuesDeEspera()); //inica la corrutina de destruir objeto.
        }
    }
    private IEnumerator DestruirPlataformasDespuesDeEspera()
    {
        yield return new WaitForSeconds(tiempoEspera);
        Destroy(gameObject);
    }

}
