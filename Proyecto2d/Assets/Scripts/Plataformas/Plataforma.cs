using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Plataforma : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    public string colorP;
 
    [Header("Solo para plataformas que cambian con las gotas")]
    [SerializeField]
    public string[] colores;
    public string nuevoColorNombre;
    public Sprite Azul;
    public Sprite Amarillo;
    public Astro astro;
    public string ColorInicial;
    public Sprite SpriteInicial;


    void Start()
    {
        ColorInicial = colorP;       
        spriteRenderer = GetComponent<SpriteRenderer>();
        SpriteInicial = spriteRenderer.sprite;
        if (astro)
        astro.Opacidad += CambioColorAstro;

    }

    public void CambiarColorAleatorio(Sprite[] coloresSprites)
    {
        if (colores.Length > 0)  //verifico si hay colores en el array
        {
            nuevoColorNombre = colores[Random.Range(0, colores.Length)]; //selecciona un nombre aleatorio del array
        }
        foreach (Sprite sprite in coloresSprites)  //recorre los sprites disponibles para enccontar el correspondiente
        {
            if (sprite.name == nuevoColorNombre) //compara el nombre del sprite con el del nuevo color sleccionado
            {
                colorP = nuevoColorNombre;     //Actualiza el string del color
                spriteRenderer.sprite = sprite;   //Cambia el sprite
                return; //sale
            }
        }

    }
    public void CambioColorGota(string color, Sprite spriteColor)
    {
        colorP = color;
        spriteRenderer.sprite = spriteColor;
    }

    public void CambioColorAstro(object sender, AstroEventArgs args)
    {
        if (!args.Sol && args.Opaco)
        {
            colorP = "Azul";
            spriteRenderer.sprite = Azul;
        }
        else if(!args.Sol && !args.Opaco)
        {
            colorP = ColorInicial;
            spriteRenderer.sprite = SpriteInicial;
        } 
        if(args.Sol && args.Opaco)
        {
            colorP = "Amarillo";
            spriteRenderer.sprite = Amarillo;
        } else if (args.Sol && !args.Opaco)
        {
            colorP = ColorInicial;
            spriteRenderer.sprite = SpriteInicial;
        }

    }


}
