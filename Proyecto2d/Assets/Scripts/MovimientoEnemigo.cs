using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoEnemigo : MonoBehaviour
{
    public float Velocity;
    private bool movimientoIzq;
    public float Minimo;
    public float Maximo;
  
    public void Start()
    {
     
    }
    private void Update()
    {
        if(this.transform.position.x >= Maximo)
        {
           movimientoIzq = true;
        }
        if(this.transform.position.x <= Minimo)
        {
            movimientoIzq = false;
        }
        if(movimientoIzq == true)
        {
            Izquiera();

        }
        else
        {
            Derecha();
        }
      
    }
    public void Izquiera()
    {
        this.transform.position += -transform. right * Velocity * Time.deltaTime;
    }
    public void Derecha()
    {
        this.transform.position += transform.right * Velocity * Time.deltaTime;
    }


}

