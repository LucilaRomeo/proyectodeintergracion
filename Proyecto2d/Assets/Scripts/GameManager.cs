using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class GameManager : MonoBehaviour
{
    public int nivelActual = 0;
    public EventHandler<int> cambioDeNivel;
    public Transform[] checkPoints;
    public Transform[] puntosIniciales;
    public static GameManager instance { get; private set; }

    private void Awake()
    {
        if(instance != null && instance != this)
        {
            Destroy(this);
        }else
        {
            instance = this;
        }
    }

    public void actualizarNivel(int nivelNuevo)
    {
        if (nivelActual == nivelNuevo)
            return;

        nivelActual = nivelNuevo;
        cambioDeNivel?.Invoke(this, nivelActual);

    
    }
    public IEnumerator MoverCamaraAlInicio(float velocidadMovimiento, float tiempoEspera)
    {
        Vector3 posicionInicial = Camera.main.transform.position; //obtiene posicion inicial de la camara
        //Creo un vector ccon la nueva posicion
        Vector3 nuevaPosicion = new Vector3(puntosIniciales[nivelActual].position.x, puntosIniciales[nivelActual].position.y, posicionInicial.z);
        float distancia = Vector3.Distance(posicionInicial, nuevaPosicion); //Calcula la distancia entre las posiciones
        float tiempoTotal = distancia / velocidadMovimiento; //Calculo tiempo total que toma mover la camara
        float tiempoPasado = 0f;

        while (tiempoPasado < tiempoTotal)
        {
            tiempoPasado += Time.deltaTime; //Actualiza tiempoPasado y calcula el valor de t para la interpolacion
            float t = Mathf.Clamp01(tiempoPasado / tiempoTotal); // hace la interpolacion
            Camera.main.transform.position = Vector3.Lerp(posicionInicial, nuevaPosicion, t);
            yield return new WaitForSeconds(0);
        }
        Camera.main.transform.position = nuevaPosicion; //Establece posicin final de la camara
        yield return new WaitForSeconds(tiempoEspera);
    }


}

