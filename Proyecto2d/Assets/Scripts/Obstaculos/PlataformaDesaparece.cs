using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformaDesaparece : MonoBehaviour
{
    public float delay = 2f;
    private Vector3 initialPosition;
    private bool isActive = true;


    private void Start()
    {
        initialPosition = transform.position; // Guardar la posici�n inicial
        GameObject.Find("PJ").GetComponent<Perder>().Restablecer += RestablecerPlataforma;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            StartCoroutine(Desaparecer());
        }
    }

    private IEnumerator Desaparecer()
    {
        yield return new WaitForSeconds(delay);
        gameObject.SetActive(false);
    }
    public void RestablecerPlataforma(object sender, bool aparece)
    {
        StopAllCoroutines();
        gameObject.SetActive(true);
        transform.position = initialPosition;
        isActive = true;
        
        
    }
}

