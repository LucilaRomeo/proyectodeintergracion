using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nubes : MonoBehaviour
{
    public GameObject[] gotitaPrefab;
    public Transform spawnPoint;
    public float intervaloMin = 1f;
    public float intervaloMax = 3f;

    void Start()
    {
        StartCoroutine(GenerarGotitas());
    }

    IEnumerator GenerarGotitas()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(intervaloMin, intervaloMax)); // Espera un tiempo aleatorio entre los intervalos especificados

            // Selecciona un prefab aleatorio del arreglo de gotitas
            GameObject gotita = gotitaPrefab[Random.Range(0, gotitaPrefab.Length)];

            // Instancia el prefab seleccionado en la posición y rotación del punto de generación
            Instantiate(gotita, spawnPoint.position, Quaternion.identity);
        }
    }
}
