using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AstroEventArgs : EventArgs
{
    public bool Opaco;
    public bool Sol;
}
public class Astro : MonoBehaviour
{
    private SpriteRenderer sprite;
    public float VelocidadAparicion = 0.5f;
    public bool Sol;
    public EventHandler<AstroEventArgs> Opacidad;

    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        TimeManager.instance.CambioCiclo += EnCambioDeCiclo;
        StartCoroutine(AparicionDesaparicion());
    }

    void Update()
    {
     
    }
    public void EnCambioDeCiclo(object sender, bool dia)
    {
        StartCoroutine(AparicionDesaparicion());
    }
    IEnumerator AparicionDesaparicion()
    {
        if((TimeManager.instance.Dia && !Sol) || (!TimeManager.instance.Dia && Sol))
             yield break;

        for (float t = 0; t <= 1; t += Time.deltaTime * VelocidadAparicion)
        {
            Color colorActual = gameObject.GetComponent<Renderer>().material.color;//Obtiene el color actual del objeto
            colorActual.a = Mathf.Lerp(0, 1, t);//Interpola (Aumenta) la opacidad gradualmente del objeto
            gameObject.GetComponent<Renderer>().material.color = colorActual; // Establece el color del material del objeto
            yield return null;
        }
        Opacidad?.Invoke(this, new AstroEventArgs { Opaco= true, Sol = this.Sol});

        yield return new WaitForSeconds(1.0f);

        for (float t = 1; t >= 0; t -= Time.deltaTime * VelocidadAparicion)
        {
            Color colorActual = gameObject.GetComponent<Renderer>().material.color; //Obtiene el color actual del objeto
            colorActual.a = Mathf.Lerp(0, 1, t); //Interpola (Disminuye) la opacidad gradualmente
            gameObject.GetComponent<Renderer>().material.color = colorActual; //Establece el color del objeto
            yield return null;
        }
        Opacidad?.Invoke(this, new AstroEventArgs { Opaco = false, Sol = this.Sol});
        
        StartCoroutine(AparicionDesaparicion());
    }
}
