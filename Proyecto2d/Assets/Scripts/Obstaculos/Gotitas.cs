using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gotitas : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    public Sprite[] Sprites;
    public string ColorGota;
    public Sprite spriteColor;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Plataforma"))
        {
            Plataforma plataforma = collision.gameObject.GetComponent<Plataforma>();
            if (plataforma != null) //Si la gota choca con una plataforma
            {
                if (spriteColor == null)
                    plataforma.CambiarColorAleatorio(Sprites);
                else
                    plataforma.CambioColorGota(ColorGota, spriteColor);
                    Destroy(gameObject);
                
            }
        }
    }
   
}
