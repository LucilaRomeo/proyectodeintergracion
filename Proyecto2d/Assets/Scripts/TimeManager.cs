using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TimeManager : MonoBehaviour
{
    public bool Dia;
    public EventHandler<bool> CambioCiclo;
    public static TimeManager instance { get; private set; }


    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }
    void Start()
    {
        GameManager.instance.cambioDeNivel += EnCambioDeNivel;
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            CambiarCiclo(!Dia);
        }
    }

    public void CambiarCiclo(bool ciclo)
    {
        Dia = ciclo;
        CambioCiclo?.Invoke(this, Dia);
    }

    public void EnCambioDeNivel(object sender, int nivel)
    {
        if(nivel == 8)
        {
            CambiarCiclo(true);
        }
    }

    
}
